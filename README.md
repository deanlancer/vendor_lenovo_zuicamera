Some files and lines may or may not be necessary. Further testing is needed.

props:
persist.vendor.camera.expose.aux=1
ro.config.zuk.has_ir_camera=true
sys.camera.packagename.zui=1
###################################
persist.vendor.camera.privapp.list=org.codeaurora.snapcam,com.zui.camera
persist.camera.privapp.list=org.codeaurora.snapcam,com.zui.camera
vendor.camera.aux.packagelist=org.codeaurora.snapcam,com.android.camera,com.zui.camera

device.mk:
#ZuiCam
$(call inherit-product-if-exists, vendor/lenovo/ZuiCamera/config.mk)

overlay config.xml:
<!-- Camera -->
<integer name="config_bioCameraApiNumber">2</integer>
<integer name="config_irCameraId">-1</integer>
<integer name="config_rgbCameraId">1</integer>